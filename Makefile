prefix ?= /usr/local

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/raven/rvn.Version=$(VERSION)"

all: \
	build/rvn

build/rvn: rvn-cli/rvn.go rvn/*.go Makefile | build
	CGO_CFLAGS="-Wno-deprecated-declarations" \
	CGO_ENABLED=1 \
		go build \
		-ldflags=${LDFLAGS} \
		-o build/rvn rvn-cli/rvn.go

build:
	mkdir build

clean:
	rm -rf build

build/raven.bash_autocomplete: build/rvn
	build/rvn autocomplete > $@

install: build/rvn build/raven.bash_autocomplete
	install -D build/rvn $(DESTDIR)$(prefix)/bin/rvn
	mkdir -p $(DESTDIR)/var/rvn/img
	mkdir -p $(DESTDIR)/var/rvn/img/user
	mkdir -p $(DESTDIR)/var/rvn/kernel
	mkdir -p $(DESTDIR)/var/rvn/initrd
	mkdir -p $(DESTDIR)/var/rvn/template
	mkdir -p $(DESTDIR)/var/rvn/ssh
	mkdir -p $(DESTDIR)/var/rvn/util
	mkdir -p $(DESTDIR)/lib/rvn
	mkdir -p $(DESTDIR)/etc/rvn
	mkdir -p $(DESTDIR)/etc/bash_completion.d
	\
	curl -L https://mirror.deterlab.net/rvn/rvn -o $(DESTDIR)/var/rvn/ssh/rvn
	chmod 0600 $(DESTDIR)/var/rvn/ssh/rvn
	curl -L https://mirror.deterlab.net/rvn/rvn.pub -o $(DESTDIR)/var/rvn/ssh/rvn.pub
	\
	install -D rvn/config.yml $(DESTDIR)/var/rvn/template/config.yml
	install -D rvn/sys.exports $(DESTDIR)/var/rvn/template/sys.exports
	install -D config/raven.conf $(DESTDIR)/etc/sysctl.d/raven.conf
	\
	install -D run_model.js $(DESTDIR)$(prefix)/lib/rvn/run_model.js
	install -D js/modeling.js $(DESTDIR)$(prefix)/lib/rvn/modeling.js
	\
	echo "{}" > $(DESTDIR)/var/rvn/run
	\
	install -D config/qemu.conf $(DESTDIR)/etc/raven/qemu.conf
	\
	curl -L https://mirror.deterlab.net/rvn/util/iamme-linux -o $(DESTDIR)/var/rvn/util/iamme-linux
	curl -L https://mirror.deterlab.net/rvn/util/iamme-freebsd -o $(DESTDIR)/var/rvn/util/iamme-freebsd
	\
	install -D build/raven.bash_autocomplete $(DESTDIR)/etc/bash_completion.d/raven
	\
	install -D bpf/v2v.c $(DESTDIR)$(prefix)/lib/rvn/v2v.c
	install -D bpf/bpf_helpers.h $(DESTDIR)$(prefix)/lib/rvn/bpf_helpers.h

# To link with local lbs use this to build
#CGO_LDFLAGS="-L /usr/local/lib" go build -o build/rvn rvn-cli/rvn.go


