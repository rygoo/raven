#!/bin/bash

set -e


if [[ $# -lt 1 ]]; then
	printf "provide distribution: [debian|ubuntu]\n"
	exit
fi
dist=$1

docker build $BUILD_ARGS -f debian/$dist.dock -t raven-builder .
docker run -v `pwd`:/raven raven-builder
