module gitlab.com/mergetb/tech/raven

go 1.21

require (
	github.com/fatih/color v1.7.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gofrs/flock v0.7.1
	github.com/iovisor/gobpf v0.2.1-0.20221005153822-16120a1bf4d4
	github.com/libvirt/libvirt-go v6.6.0+incompatible
	github.com/libvirt/libvirt-go-xml v6.6.0+incompatible
	github.com/mergetb/go-ping v0.0.0-20181106132940-7b3a8e3ebce2
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/cobra v0.0.3
	gitlab.com/mergetb/tech/rtnl v0.1.8
)

require (
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mdlayher/netlink v0.0.0-20190617153422-f82a9b10b2bc // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.30.0 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/term v0.13.0 // indirect
	google.golang.org/protobuf v1.32.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
