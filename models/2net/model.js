/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 2 node system
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

zwitch = {
  'name': 'nimbus',
  'image': 'cumulusvx-3.5',
  'os': 'linux',
  'defaultdisktype': { 'bus': 'virtio', 'dev': 'vda' },
  'cpu': {
    'cores': 1
  },
  'memory': {
    'capacity': GB(2)
  },
  'mounts': [
    { 'source': env.PWD, 'point': '/tmp/here' }
  ],
};

nodes = Range(2).map(i => ({
  'name': `n${i}`,
  'image': 'ubuntu-1804',
  'os': 'linux',
  'cpu': {
    'cores': 4
  },
  'memory': {
    'capacity': GB(6)
  },
  'mounts': [
    { 'source': env.HOME, 'point': '/tmp/home' }
  ],
  'disks': [
    { 'size': "10G", 'dev': 'vdd', 'bus': 'virtio' },
    { 'size': "5G", 'dev': 'vde', 'bus': 'virtio' },
  ]
}));

links = [
  ...Range(2).map(i => Link(`n${i}`, 0, 'nimbus', i+3)),
]

topo = {
  'name': '2net',
  'nodes':[...nodes],
  'switches': [zwitch],
  'links': links
};
