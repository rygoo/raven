package rvn

// TODO
//   - better error diagnostics and propagation

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"os/exec"
	"sort"
	"strconv"
	"strings"

	"github.com/fatih/color"
	libvirt "github.com/libvirt/libvirt-go"
	xlibvirt "github.com/libvirt/libvirt-go-xml"
	log "github.com/sirupsen/logrus"
)

// Types ======================================================================

// DomStatus encapsulates various information about a libvirt domain for
// purposes of serialization and presentation.
type DomStatus struct {
	Name        string
	State       string
	ConfigState string
	IP          string
	Macs        []string
	VNC         int
}

// LinkStatus holds the status of a link.
type LinkStatus struct {
	State  string
	Bridge string
	Device string
	Link   Link
}

const (
	// ravenInfraNetwork is the default network that is created for
	// all raven nodes to connect to and be given an infrastructure
	// network with dhcp addressing.
	ravenInfraNetwork = "raven-infra"
)

// Variables ==================================================================

var (
	conn *libvirt.Connect
)

// Functions ==================================================================

// Create creates a libvirt definition for the supplied topology.  It does not
// launch the system. For that functionality use the Launch function. If a
// topology with the same name as the topology provided as an argument exists,
// that topology will be overwritten by the system generated from the argument.
func Create() {
	//TODO need to return an error if shizz goes sideways

	wd, err := WkDir()
	if err != nil {
		log.Fatalf("create: failed to get working dir")
	}

	topo, err := LoadTopo()
	if err != nil {
		log.Fatalf("create failed to load topo")
	}

	topoDir := wd
	os.MkdirAll(topoDir, 0755)

	doms := make(map[string]*xlibvirt.Domain)
	nets := make(map[string]*xlibvirt.Network)

	subnet := LoadRuntime().AllocateSubnet(topo.Name) + topo.SubnetOffset
	topo.MgmtIP = fmt.Sprintf("172.22.%d.1", subnet)

	n := &xlibvirt.Network{
		Name: topo.QualifyName(ravenInfraNetwork),
		IPs: []xlibvirt.NetworkIP{
			xlibvirt.NetworkIP{
				Address: topo.MgmtIP,
				Netmask: "255.255.255.0",
				DHCP: &xlibvirt.NetworkDHCP{
					Ranges: []xlibvirt.NetworkDHCPRange{
						xlibvirt.NetworkDHCPRange{
							Start: fmt.Sprintf("172.22.%d.2", subnet),
							End:   fmt.Sprintf("172.22.%d.254", subnet),
						},
					},
				},
			},
		},
		Domain: &xlibvirt.NetworkDomain{
			Name:      topo.Name + ".net",
			LocalOnly: "yes",
		},
		Forward: &xlibvirt.NetworkForward{
			Mode: "nat",
		},
	}

	if len(topo.Aliases) > 0 {
		n.DnsmasqOptions = new(xlibvirt.NetworkDnsmasqOptions)
		for _, alias := range topo.Aliases {
			n.DnsmasqOptions.Option = append(
				n.DnsmasqOptions.Option, xlibvirt.NetworkDnsmasqOption{
					Value: fmt.Sprintf(
						"cname=%s.%s.net,%s.%s.net",
						alias.Alias, topo.Name,
						alias.Name, topo.Name,
					),
				})
		}
	}

	nets[ravenInfraNetwork] = n

	for _, node := range topo.Nodes {
		d, err := newDom(&node.Host, &topo)
		if err != nil {
			log.Fatal(err)
		}
		GenConfig(node.Host, topo)
		doms[node.Name] = d
		if !node.NoTestNet {
			domConnect(topo.QualifyName(ravenInfraNetwork), nil, &node.Host, d, nil)
		}
	}

	for _, zwitch := range topo.Switches {
		d, err := newDom(&zwitch.Host, &topo)
		if err != nil {
			log.Fatal(err)
		}
		GenConfig(zwitch.Host, topo)
		doms[zwitch.Name] = d
		if !zwitch.NoTestNet {
			domConnect(topo.QualifyName(ravenInfraNetwork), nil, &zwitch.Host, d, nil)
		}
	}

	for _, link := range topo.Links {

		mtu := linkMtu(&link)

		if link.IsTap() {
			// check that this hostname is actually a host
			host := link.Endpoints[0].Name
			_, ok := doms[host]
			if !ok {
				log.Fatal("host does not exist for tap")
			}

			bridge, err := link.GetBridge()
			if err != nil {
				log.Fatal(err)
			}

			if bridge == ravenInfraNetwork {
				log.Fatal(
					"bridge name cannot be called %s, already in use.",
					ravenInfraNetwork,
				)
			}

			// only create the bridge network once.
			_, ok = nets[bridge]
			if !ok {
				// for all other links create a networked back bridge
				n := &xlibvirt.Network{
					Name:                topo.QualifyName(bridge),
					TrustGuestRxFilters: "yes",
					IPv6:                "yes",
					MTU:                 &xlibvirt.NetworkMTU{Size: mtu},
					Bridge: &xlibvirt.NetworkBridge{
						Delay: "0",
						STP:   "off",
						Name:  bridge,
					},
					Domain: &xlibvirt.NetworkDomain{
						Name: topo.Name + "." + bridge,
					},
				}

				nets[bridge] = n
			}

			// for each interface connected to the bridge, we need to add that
			// dom's interface
			tapIface := xlibvirt.DomainInterface{
				Managed: "no",
				Source: &xlibvirt.DomainInterfaceSource{
					Bridge: &xlibvirt.DomainInterfaceSourceBridge{
						Bridge: bridge,
					},
				},
				Target: &xlibvirt.DomainInterfaceTarget{
					Dev: fmt.Sprintf("%s", link.Name),
				},
				Model: &xlibvirt.DomainInterfaceModel{
					Type: "virtio",
				},
				MTU: &xlibvirt.DomainInterfaceMTU{Size: mtu},
			}

			// add the interfaces (L2) to the host
			doms[host].Devices.Interfaces = append(
				doms[host].Devices.Interfaces, tapIface,
			)

		} else {
			// for all other links create a networked back bridge
			n := &xlibvirt.Network{
				Name:   topo.QualifyName(link.Name),
				IPv6:   "yes",
				MTU:    &xlibvirt.NetworkMTU{Size: mtu},
				Bridge: &xlibvirt.NetworkBridge{Delay: "0", STP: "off"},
			}
			nets[link.Name] = n
		}
	}

	resolveLinks(&topo)

	for _, x := range topo.Nodes {
		for _, p := range x.ports {
			link := topo.GetLink(p.Link)
			if link.IsTap() {
				continue
			}
			domConnect(
				topo.QualifyName(p.Link),
				link,
				&x.Host,
				doms[x.Name],
				link.Props,
			)
		}
	}

	for _, x := range topo.Switches {
		for _, p := range x.ports {
			link := topo.GetLink(p.Link)
			if link.IsTap() {
				continue
			}
			domConnect(
				topo.QualifyName(p.Link),
				link,
				&x.Host,
				doms[x.Name],
				link.Props,
			)
		}
	}

	data, _ := json.MarshalIndent(topo, "", "  ")
	ioutil.WriteFile(topoDir+"/"+TopoFile, []byte(data), 0644)

	checkConnect()

	for _, d := range doms {
		xml, err := d.Marshal()
		if err != nil {
			log.Printf("error marshalling domain %v", err)
		}
		ioutil.WriteFile(topoDir+"/dom_"+d.Name+".xml", []byte(xml), 0644)

		dd, err := conn.LookupDomainByName(d.Name)
		if err != nil {
			_, err := conn.DomainDefineXML(xml)
			if err != nil {
				log.Fatalf("%s error defining domain %v", d.Name, err)
			}
		} else {
			dd.Destroy()
			dd.Undefine()
			conn.DomainDefineXML(xml)
			dd.Free()
		}
	}

	for _, n := range nets {
		xml, _ := n.Marshal()
		ioutil.WriteFile(topoDir+"/net_"+n.Name+".xml", []byte(xml), 0644)

		nn, err := conn.LookupNetworkByName(n.Name)
		if err != nil {
			_, err := conn.NetworkDefineXML(xml)
			if err != nil {
				log.Fatalf("%s error defining network %v", n.Name, err)
			}
		} else {
			nn.Destroy()
			nn.Undefine()
			conn.NetworkDefineXML(xml)
			nn.Free()
		}
	}

	//create NFS exports
	ExportNFS(topo)

}

// Destroy completely wipes out a topology with the given name. If the system
// is running within libvirt it is torn down. The entire definition of the
// system is also removed from libvirt.
func Destroy() {
	//TODO return error on sideways
	checkConnect()
	dbCheckConnection()

	wd, err := WkDir()
	if err != nil {
		log.Printf("newdom: failed to get working dir")
		return
	}

	topo, err := LoadTopo()
	if err != nil {
		//log.Printf("destroy: failed to load topo - %v", err)
		//nothing to destroy
		return
	}
	topoDir := wd
	RemoveTopo()
	os.RemoveAll(topoDir)

	for _, x := range topo.Nodes {
		ds, err := DomainStatus(topo.Name, x.Name)
		if err == nil {
			cleanupHostKey(ds.IP)
		}
		destroyDomain(topo.QualifyName(x.Name), conn)
		if x.Host.TelnetPort != 0 {
			LoadRuntime().FreeTelnetPort(x.Host.TelnetPort)
		}
		stateKey := fmt.Sprintf("config_state:%s:%s", topo.Name, x.Name)
		db.Del(stateKey)
	}
	for _, x := range topo.Switches {
		ds, err := DomainStatus(topo.Name, x.Name)
		if err == nil {
			cleanupHostKey(ds.IP)
		}
		if x.Host.TelnetPort != 0 {
			LoadRuntime().FreeTelnetPort(x.Host.TelnetPort)
		}
		destroyDomain(topo.QualifyName(x.Name), conn)
		stateKey := fmt.Sprintf("config_state:%s:%s", topo.Name, x.Name)
		db.Del(stateKey)
	}

	tap := make(map[string]bool)
	for _, x := range topo.Links {
		cleanupLinkNetwork(topo.QualifyName(x.Name), conn)
		if !x.IsTap() {
			destroyNetwork(topo.QualifyName(x.Name), conn)
		}
		if x.IsTap() {
			bridge, err := x.GetBridge()
			if err != nil {
				continue
			}
			_, ok := tap[bridge]
			if !ok {
				tap[bridge] = true
				destroyNetwork(topo.QualifyName(bridge), conn)
			}
		}
	}

	cleanupTestNetwork(topo.QualifyName(ravenInfraNetwork), conn)
	destroyNetwork(topo.QualifyName(ravenInfraNetwork), conn)
	LoadRuntime().FreeSubnet(topo.Name)
	UnexportNFS(topo.Name)
}

// Shutdown turns of a virtual machine gracefully
func Shutdown() []error {
	checkConnect()
	dbCheckConnection()

	topo, err := LoadTopo()
	if err != nil {
		if strings.Contains(err.Error(), "topo.json: no such file or directory") {
			log.Printf("Topology not built. Use `rvn build` first")
			return []error{}
		}
		return []error{fmt.Errorf("shutdown: failed to load topo")}
	}

	errs := []error{}
	for _, x := range topo.Nodes {
		err := shutdownDomain(topo.QualifyName(x.Name), conn)
		if err != nil {
			errs = append(errs, err)
		}
	}
	for _, x := range topo.Switches {
		err := shutdownDomain(topo.QualifyName(x.Name), conn)
		if err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// WipeNode deletes a nodes virtual hard disk, create's a new one, and
// re-launches the VM.
func WipeNode(topo Topo, name string) error {
	checkConnect()
	dbCheckConnection()
	h := topo.GetHost(name)
	if h == nil {
		return fmt.Errorf("host %s does not exist", name)
	}
	destroyDomain(topo.QualifyName(name), conn)
	d, err := newDom(h, &topo)
	if err != nil {
		return err
	}

	if !h.NoTestNet {
		domConnect(topo.QualifyName(ravenInfraNetwork), nil, h, d, nil)
	}
	resolveLinks(&topo)
	for _, p := range h.ports {
		lnk := topo.GetLink(p.Link)
		domConnect(topo.QualifyName(p.Link), lnk, h, d, topo.GetLink(p.Link).Props)
	}

	xml, err := d.Marshal()
	if err != nil {
		return fmt.Errorf("error marshalling domain %v", err)
	}

	dom, err := conn.DomainDefineXML(xml)
	if err != nil {
		return fmt.Errorf("error defining domain %v", err)
	}

	err = dom.Create()
	if err != nil {
		return err
	}

	for _, p := range h.ports {
		lnk := topo.GetLink(p.Link)

		if lnk.V2V {
			ntwk, err := conn.LookupNetworkByName(topo.QualifyName(p.Link))
			if err != nil {
				return fmt.Errorf("unable to find network %v", err)
			}
			v2v(ntwk)
			ntwk.Free()
		}
	}
	return nil
}

// Launch brings up the system with the given name. This system must exist
// visa-vis the create function before calling Launch. The return value is
// a list of diagnostic strings that were provided by libvirt when launching
// the system. The existence of diagnostics does not necessarily indicate
// an error in launching. This function is asynchronous, when it returns the
// system is still launching. Use the Status function to check up on a the
// launch process.
func Launch() []string {
	//TODO name should probably be something more like 'deploy'
	checkConnect()

	topo, err := LoadTopo()
	if err != nil {
		if strings.Contains(err.Error(), "topo.json: no such file or directory") {
			log.Printf("Topology not built. Use `rvn build` first")
			return []string{}
		}
		err := fmt.Errorf("failed to load topo %v", err)
		return []string{fmt.Sprintf("%v", err)}
	}

	//collect all the doamins and networks first so we know everything we need
	//exists
	var errors []string
	var doms []*libvirt.Domain
	nets := make(map[*libvirt.Network]Link)

	for _, x := range topo.Nodes {
		d, err := conn.LookupDomainByName(topo.QualifyName(x.Name))
		if err != nil {
			errors = append(errors, fmt.Sprintf("%s: %v", x.Name, err))
		} else {
			doms = append(doms, d)
		}
	}
	for _, x := range topo.Switches {
		d, err := conn.LookupDomainByName(topo.QualifyName(x.Name))
		if err != nil {
			errors = append(errors, fmt.Sprintf("%s: %v", x.Name, err))
		} else {
			doms = append(doms, d)
		}
	}

	// tap ensures we only add the network once per bridge
	tap := make(map[string]bool)
	for _, x := range topo.Links {
		switch {
		case x.IsTap():
			// if the link is a part of tap, we need to create a single
			// network which we can add all the taps to
			bridge, err := x.GetBridge()
			if err != nil {
				errors = append(errors, fmt.Sprintf("%s: %v", x.Name, err))
				break
			}

			n, err := conn.LookupNetworkByName(topo.QualifyName(bridge))
			if err != nil {
				errors = append(errors, fmt.Sprintf("%s: %v", x.Name, err))
				break
			}

			_, ok := tap[bridge]
			if !ok {
				tap[bridge] = true
				nets[n] = Link{}
			}
		default:
			n, err := conn.LookupNetworkByName(topo.QualifyName(x.Name))
			if err != nil {
				errors = append(errors, fmt.Sprintf("%s: %v", x.Name, err))
			} else {
				nets[n] = x
			}
		}
	}

	// raven infra network
	n, err := conn.LookupNetworkByName(topo.QualifyName(ravenInfraNetwork))
	allowRPCBind(n)
	if err != nil {
		errors = append(errors, fmt.Sprintf("%s: %v", ravenInfraNetwork, err))
	} else {
		nets[n] = Link{}
	}

	for net, link := range nets {
		active, err := net.IsActive()
		if err == nil && !active {
			err := net.Create()
			name, _ := net.GetName()
			if err != nil {
				errors = append(errors, fmt.Sprintf("%s: %v", name, err))
			}
			if name != topo.QualifyName(ravenInfraNetwork) {
				setBridgeProperties(net, link)
			}
		}
	}

	for _, dom := range doms {
		active, err := dom.IsActive()
		if err == nil && !active {
			err := dom.Create()
			if err != nil {
				name, _ := dom.GetName()
				errors = append(errors, fmt.Sprintf("%s: %v", name, err))
			}
			dom.Free()
		}
	}

	// must be done after doms are activated, otherwise interfaces will not be
	// there
	for net, link := range nets {
		if link.V2V {
			v2v(net)
		}

		net.Free()
	}

	return errors
}

// DomainInfo fetches the libvirt domain information about host within a
// raven topology
func DomainInfo(topo, name string) (*xlibvirt.Domain, error) {

	checkConnect()
	dom, err := conn.LookupDomainByName(topo + "_" + name)
	if err != nil {
		return nil, err
	}

	xmldoc, err := dom.GetXMLDesc(0)
	if err != nil {
		return nil, err
	}

	xdom := &xlibvirt.Domain{}
	err = xdom.Unmarshal(xmldoc)
	if err != nil {
		return nil, err
	}

	return xdom, nil

}

// Status returns the runtime status of a topology, node by node and network by
// network.
func Status() map[string]interface{} {

	status := make(map[string]interface{})

	checkConnect()

	topo, err := LoadTopo()
	if err != nil {
		if strings.Contains(err.Error(), "topo.json: no such file or directory") {
			log.Printf("Topology not built. Use `rvn build` first")
			return nil
		}
		log.Printf("status: failed to load topo - %v", err)
		return nil
	}

	nodes := make(map[string]DomStatus)
	status["nodes"] = nodes

	switches := make(map[string]DomStatus)
	status["switches"] = switches

	links := make(map[string]LinkStatus)
	status["links"] = links

	for _, x := range topo.Nodes {
		nodes[x.Name] =
			domainStatus(topo.Name, x.Name, topo.QualifyName(x.Name), conn)
	}
	for _, x := range topo.Switches {
		switches[x.Name] =
			domainStatus(topo.Name, x.Name, topo.QualifyName(x.Name), conn)
	}

	for _, x := range topo.Links {
		links[x.Name] = networkStatus(topo.QualifyName(x.Name), conn, x)
	}

	subnet, ok := LoadRuntime().SubnetReverseTable[topo.Name]
	if ok {
		status["mgmtip"] = fmt.Sprintf("172.22.%d.1", subnet)
	}

	updateTopoAnsibleHosts(topo.Name, nodes, switches)

	return status
}

// DomainStatus returns the current state of a domain (host) within a libvirt
// topology.
func DomainStatus(topo, name string) (DomStatus, error) {
	checkConnect()
	return domainStatus(topo, name, topo+"_"+name, conn), nil
}

// Reboot attempts to gracefully reboot a raven host
func Reboot(rr RebootRequest) error {
	checkConnect()

	for _, x := range rr.Nodes {

		d, err := conn.LookupDomainByName(fmt.Sprintf("%s_%s", rr.Topo, x))
		if err != nil {
			continue
		}

		if rr.Reset {
			d.Reset(0)
		} else {
			d.Reboot(libvirt.DOMAIN_REBOOT_DEFAULT)
		}

	}

	return nil
}

// Powerup turns on a VM
func Powerup(nodes []string) error {
	checkConnect()

	topo, err := LoadTopo()
	if err != nil {
		return err
	}

	for _, x := range nodes {

		d, err := conn.LookupDomainByName(fmt.Sprintf("%s_%s", topo.Name, x))
		if err != nil {
			continue
		}

		d.Create()

	}

	return nil
}

// Powerdown turns off a VM
func Powerdown(nodes []string, force bool) error {
	checkConnect()

	topo, err := LoadTopo()
	if err != nil {
		return err
	}

	for _, x := range nodes {

		id := fmt.Sprintf("%s_%s", topo.Name, x)
		d, err := conn.LookupDomainByName(id)
		if err != nil {
			continue
		}

		log.Printf("powering down %s", id)

		if force {
			d.Destroy()
		} else {
			d.Shutdown()
		}

	}

	return nil
}

// Helper Functions ===========================================================

func connect() {
	var err error
	conn, err = libvirt.NewConnect("qemu:///session")
	if err != nil {
		log.Printf("libvirt connect failure: %v", err)
	}
}

func isAlive() bool {
	result, err := conn.IsAlive()
	if err != nil {
		log.Printf("error assesing connection liveness - %v", err)
		return false
	}
	return result
}

func checkConnect() {
	for conn == nil {
		connect()
	}

	for !isAlive() {
		connect()
	}
}

func newDom(h *Host, t *Topo) (*xlibvirt.Domain, error) {

	switch h.Platform {

	case "x86_64":
		return x86Dom(h, t)

	case "arm7":
		return arm7Dom(h, t)

	default:
		log.Printf("unrecognized platform - using x86_64")
		return x86Dom(h, t)

	}
}

func createModel(h *Host) *xlibvirt.DomainCPUModel {
	if h.CPU.Model == "" {
		return nil
	}
	return &xlibvirt.DomainCPUModel{Value: h.CPU.Model}
}

func createFeature(h *Host) []xlibvirt.DomainCPUFeature {
	switch h.CPU.Model {
	case "":
		return nil
	case "qemu64":
		return []xlibvirt.DomainCPUFeature{
			xlibvirt.DomainCPUFeature{
				Name:   "svm",
				Policy: "disable",
			},
		}
	}

	return nil
}

func x86Dom(h *Host, t *Topo) (*xlibvirt.Domain, error) {

	instanceImage, err := createImage(h)
	if err != nil {
		return nil, err
	}

	disks := defaultDisk(h, instanceImage)
	disks, err = appendDisks(h, disks)
	if err != nil {
		return nil, err
	}

	passthru := ""
	migration := ""
	if h.CPU.Passthru {
		passthru = "host-passthrough"
		migration = "off"
	}

	var iommu *xlibvirt.DomainIOMMU
	if h.Iommu {
		iommu = &xlibvirt.DomainIOMMU{
			Model: "intel",
			Driver: &xlibvirt.DomainIOMMUDriver{
				IntRemap: "on",
			},
		}
	}

	var clock *xlibvirt.DomainClock
	if h.VirtualClock {
		clock = &xlibvirt.DomainClock{
			Offset: "utc",
			Timer: []xlibvirt.DomainTimer{{
				Name:  "rtc",
				Track: "guest",
			}},
		}
	}

	d := &xlibvirt.Domain{
		Type: "kvm",
		Name: t.QualifyName(h.Name),
		Features: &xlibvirt.DomainFeatureList{
			ACPI: &xlibvirt.DomainFeature{},
			APIC: &xlibvirt.DomainFeatureAPIC{},
		},
		OS: &xlibvirt.DomainOS{
			Type:    &xlibvirt.DomainOSType{Type: "hvm", Arch: "x86_64", Machine: h.Machine},
			Kernel:  findKernel(h),
			Initrd:  findInitrd(h),
			Loader:  findSysFirmware(h),
			Cmdline: h.Cmdline,
		},
		CPU: &xlibvirt.DomainCPU{

			Match: "minimum",
			//TODO: plumb this capability in ....
			Mode:       passthru,
			Migratable: migration,
			Model:      createModel(h),
			Topology: &xlibvirt.DomainCPUTopology{
				Sockets: h.CPU.Sockets,
				Cores:   h.CPU.Cores,
				Threads: h.CPU.Threads,
			},
			Features: createFeature(h),
		},
		VCPU: &xlibvirt.DomainVCPU{
			Value: uint(h.CPU.Sockets * h.CPU.Cores * h.CPU.Threads),
		},
		Memory: &xlibvirt.DomainMemory{
			Value: uint(h.Memory.Capacity.Value),
			Unit:  h.Memory.Capacity.Unit,
		},
		Devices: &xlibvirt.DomainDeviceList{
			Emulator: h.Emulator,
			Serials:  createSerials(h),
			Consoles: []xlibvirt.DomainConsole{
				xlibvirt.DomainConsole{
					Source: &xlibvirt.DomainChardevSource{
						Pty: &xlibvirt.DomainChardevSourcePty{},
					},
					Target: &xlibvirt.DomainConsoleTarget{Type: "serial"},
				},
			},
			IOMMU: iommu,
			Graphics: []xlibvirt.DomainGraphic{
				createGraphics(t),
			},
			Disks:  disks,
			Videos: getDomainVideos(h),
			RNGs: []xlibvirt.DomainRNG{
				xlibvirt.DomainRNG{
					Model: "virtio",
					Backend: &xlibvirt.DomainRNGBackend{
						Random: &xlibvirt.DomainRNGBackendRandom{
							Device: "/dev/urandom",
						},
					},
				},
			},
		},
		Clock: clock,
	}

	return d, nil

}

func createSerials(h *Host) []xlibvirt.DomainSerial {

	var serials []xlibvirt.DomainSerial
	if h.OS != "onie" {
		serials = append(serials, xlibvirt.DomainSerial{
			Source: &xlibvirt.DomainChardevSource{
				Pty: &xlibvirt.DomainChardevSourcePty{},
			},
		})

	}

	// serials = append(serials,
	// 	xlibvirt.DomainSerial{
	// 		Source: &xlibvirt.DomainChardevSource{
	// 			TCP: &xlibvirt.DomainChardevSourceTCP{
	// 				Mode:    "bind",
	// 				Host:    "localhost",
	// 				Service: fmt.Sprintf("%d", h.TelnetPort),
	// 			},
	// 		},
	// 		Protocol: &xlibvirt.DomainChardevProtocol{
	// 			Type: "telnet",
	// 		},
	// 	})

	return serials

}

func createGraphics(t *Topo) xlibvirt.DomainGraphic {
	switch t.Options.Display {
	case "local":
		return xlibvirt.DomainGraphic{
			Desktop: &xlibvirt.DomainGraphicDesktop{
				Display: "gtk",
			},
		}
	default:
		return xlibvirt.DomainGraphic{
			VNC: &xlibvirt.DomainGraphicVNC{
				Port:     -1,
				AutoPort: "yes",
			},
		}
	}
}

func createImage(h *Host) (string, error) {

	wd, err := WkDir()
	if err != nil {
		return "", fmt.Errorf("newdom: failed to get working dir")
	}

	log.Debugf("parsed image: %s", h.Image)
	// location of baseImage depends on how image was imported
	baseImage := "/var/rvn/img/"
	// the instance name/location will depend on how it is parsed
	// if name is empty, then load netboot from /rvn/img
	if h.Image == "" {
		baseImage += "netboot"
	} else if len(strings.Split(h.Image, "/")) > 1 {
		// if name points to a local path or to url
		baseImage += "user/"
		parsedURL, err := url.Parse(h.Image)
		if err != nil {
			return "", fmt.Errorf("error validating URL: %v", err)
		}
		remoteHost := parsedURL.Host
		// if remoteHost is empty, its a local image
		if remoteHost == "" {
			// check the image name as a relative or absolute path
			_, err := os.Stat(h.Image)
			// then check if it exists in user/ directory
			if err != nil {
				log.Debugf("Image does not exist as path, check /user: %v", err)
				path := strings.Split(h.Image, "/")
				baseImage += path[len(path)-1]
				_, err := os.Stat(baseImage)
				if err != nil {
					return "", fmt.Errorf("image cannot be found locally: %s", h.Image)
				}
			} else {
				baseImage = h.Image
			}
		} else {
			subPath, imageName, err := ParseURL(parsedURL)
			if err != nil {
				return "", fmt.Errorf("error parsing URL: %v", err)
			}
			baseImage += subPath + imageName
		}
		// this only leaves names, which default to deterlab and /rvn/img location
	} else {
		baseImage += h.Image
	}

	instanceImage := wd + "/" + h.Name
	os.RemoveAll(instanceImage)

	var out []byte
	if h.DedicatedDisk {
		out, err = exec.Command(
			"qemu-img",
			"convert",
			"-O", "qcow2",
			"-o", "lazy_refcounts=on,preallocation=falloc",
			baseImage,
			instanceImage).CombinedOutput()
	} else {
		out, err = exec.Command(
			"qemu-img",
			"create",
			"-f",
			"qcow2",
			"-o", "backing_file="+baseImage,
			"-F",
			"qcow2",
			instanceImage).CombinedOutput()
	}

	if err != nil {
		return "", fmt.Errorf("error creating image file for %s: %v", h.Name, string(out))
	}

	return instanceImage, nil

}

func defaultDisk(h *Host, instanceImage string) []xlibvirt.DomainDisk {
	return []xlibvirt.DomainDisk{
		xlibvirt.DomainDisk{
			Device: "disk",
			Driver: &xlibvirt.DomainDiskDriver{
				Name:  "qemu",
				Type:  "qcow2",
				Cache: h.DiskCache,
			},
			Source: &xlibvirt.DomainDiskSource{
				File: &xlibvirt.DomainDiskSourceFile{
					File: instanceImage,
				},
			},
			Target: &xlibvirt.DomainDiskTarget{
				Dev: h.DefaultDisktype.Dev,
				Bus: h.DefaultDisktype.Bus,
			},
			Boot: &xlibvirt.DomainDeviceBoot{
				Order: DefaultDiskBootOrder,
			},
		},
	}
}

func checkValidSource(source string) error {
	_, err := os.Stat(source)
	return err
}

func appendDisks(h *Host, disks []xlibvirt.DomainDisk) ([]xlibvirt.DomainDisk, error) {
	if len(h.Disks) > 0 {
		for i, d := range h.Disks {
			var boot *xlibvirt.DomainDeviceBoot
			if d.BootOrder != 0 {
				boot = &xlibvirt.DomainDeviceBoot{
					Order: uint(d.BootOrder),
				}
			}
			err := checkValidSource(h.Disks[i].Source)
			if err != nil {
				log.Errorf("Disk: %v", h.Disks[i])
				log.Error(err)
				return nil, err
			}
			disks = append(disks, xlibvirt.DomainDisk{
				Device: "disk",
				Driver: &xlibvirt.DomainDiskDriver{Name: "qemu", Type: "qcow2"},
				Source: &xlibvirt.DomainDiskSource{
					File: &xlibvirt.DomainDiskSourceFile{
						File: h.Disks[i].Source,
					},
				},
				Target: &xlibvirt.DomainDiskTarget{
					Dev: h.Disks[i].Dev,
					Bus: h.Disks[i].Bus,
				},
				Boot: boot,
			})
		}
	}
	return disks, nil
}

func arm7Dom(h *Host, t *Topo) (*xlibvirt.Domain, error) {

	instanceImage, err := createImage(h)
	if err != nil {
		return nil, err
	}

	disks := defaultDisk(h, instanceImage)
	disks, err = appendDisks(h, disks)
	if err != nil {
		return nil, err
	}

	// construct the vm
	d := &xlibvirt.Domain{
		Type: "qemu",
		Name: t.QualifyName(h.Name),
		OS: &xlibvirt.DomainOS{
			Type: &xlibvirt.DomainOSType{
				Type:    "hvm",
				Arch:    h.Arch,
				Machine: h.Machine,
			},
			Kernel: findKernel(h),
		},
		CPU: &xlibvirt.DomainCPU{
			Mode:  "custom",
			Match: "exact",
			Topology: &xlibvirt.DomainCPUTopology{
				Sockets: h.CPU.Sockets,
				Cores:   h.CPU.Cores,
				Threads: h.CPU.Threads,
			},
			Model: &xlibvirt.DomainCPUModel{
				Value: h.CPU.Model,
			},
		},
		VCPU: &xlibvirt.DomainVCPU{
			Value: uint(h.CPU.Sockets * h.CPU.Cores * h.CPU.Threads),
		},
		Memory: &xlibvirt.DomainMemory{
			Value: uint(h.Memory.Capacity.Value),
			Unit:  h.Memory.Capacity.Unit,
		},
		Devices: &xlibvirt.DomainDeviceList{
			Emulator: h.Emulator,
			Serials: []xlibvirt.DomainSerial{
				xlibvirt.DomainSerial{
					Source: &xlibvirt.DomainChardevSource{
						TCP: &xlibvirt.DomainChardevSourceTCP{
							Mode:    "bind",
							Host:    "localhost",
							Service: fmt.Sprintf("%d", h.TelnetPort),
						},
					},
					Protocol: &xlibvirt.DomainChardevProtocol{
						Type: "telnet",
					},
				},
			},
			Disks:  disks,
			Videos: getDomainVideos(h),
		},
	}

	return d, nil

}

func getDomainVideos(h *Host) []xlibvirt.DomainVideo {

	if h.Video != "" {
		return []xlibvirt.DomainVideo{
			xlibvirt.DomainVideo{
				Model: xlibvirt.DomainVideoModel{Type: h.Video},
			},
		}
	}

	if h.OS == "onie" {
		return []xlibvirt.DomainVideo{
			xlibvirt.DomainVideo{
				Model: xlibvirt.DomainVideoModel{Type: "std"},
			},
		}
	}
	return []xlibvirt.DomainVideo{}

}

func nextPort(min, max int) int {

	ports := portsInUse(min, max)
	ports = append(ports, min-1)

	port := nextInt(ports)

	if port > max {
		log.Printf(
			"warning: first available port is beyond specified max %d", port)
	}

	return port
}

func portsInUse(from, to int) []int {

	cmd := exec.Command(
		"ss", "-lnt", fmt.Sprintf("( sport >= %d and sport < %d )", from, to))
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(string(out))
		log.Fatal(err)
	}

	var result []int
	lines := strings.Split(string(out), "\n")
	lines = lines[1 : len(lines)-1]
	for _, line := range lines {
		fields := strings.Fields(line)
		local := fields[3]
		parts := strings.Split(local, ":")
		port, _ := strconv.Atoi(parts[len(parts)-1])
		result = append(result, port)
	}

	return result

}

func nextInt(ports []int) int {

	sort.Ints(ports)
	for i := 0; i < len(ports)-1; i++ {
		if ports[i+1] > ports[i]+1 {
			return ports[i] + 1
		}
	}

	return ports[len(ports)-1] + 1

}

func nextIntFrom(ports []int, start int) int {

	if len(ports) == 0 {
		return start
	}

	sort.Ints(ports)
	for i := 0; i < len(ports)-1; i++ {
		if ports[i+1] > ports[i]+1 {
			return ports[i] + 1
		}
	}

	return ports[len(ports)-1] + 1

}

func domConnect(
	net string, l *Link, h *Host, dom *xlibvirt.Domain, props map[string]interface{},
) {

	var boot *xlibvirt.DomainDeviceBoot
	if strings.ToLower(h.OS) == "netboot" {
		if props != nil {
			bootOrder, ok := props["boot"]
			if ok {
				bootOrderNum, ok := bootOrder.(float64)
				if ok {
					boot = &xlibvirt.DomainDeviceBoot{
						Order: uint(bootOrderNum),
					}
				}
			}
		}
	}

	var ifmac *xlibvirt.DomainInterfaceMAC
	if props != nil {
		_macs, ok := props["mac"]
		if ok {
			macs, ok := _macs.(map[string]interface{})
			if ok {
				_mac, ok := macs[h.Name]
				if ok {
					mac, ok := _mac.(string)
					if ok {
						ifmac = &xlibvirt.DomainInterfaceMAC{
							Address: mac,
						}
					}
				}
			}
		}
	}

	hostNicModel := h.DefaultNic
	hostInterfaceDriver := "vhost"
	var target *xlibvirt.DomainInterfaceTarget

	_hostProps, ok := props[h.Name]
	if ok {
		hostProps, ok := _hostProps.(map[string]interface{})
		if ok {
			_hostNic, ok := hostProps["nic"]
			if ok {
				// virtio
				hostNic, ok := _hostNic.(string)
				if ok {
					hostNicModel = hostNic
				}
			}
		}
	}

	var driver *xlibvirt.DomainInterfaceDriver
	// for v2v links, disable all hardware offloading to prevent problems with bad checksums
	if l != nil && l.V2V {
		driver = &xlibvirt.DomainInterfaceDriver{
			Name: hostInterfaceDriver,
			// need to have twice the number of queues as procs for eBPF muffins
			//Queues: uint(h.CPU.Sockets * h.CPU.Cores * h.CPU.Threads * 2),
			Host: &xlibvirt.DomainInterfaceDriverHost{
				CSum: "off",
				TSO4: "off",
				TSO6: "off",
				//ECN:  "off",
				//UFO:  "off",
			},
			Guest: &xlibvirt.DomainInterfaceDriverGuest{
				CSum: "off",
				TSO4: "off",
				TSO6: "off",
				//ECN:  "off",
				//UFO:  "off",
			},
		}
	} else {
		driver = &xlibvirt.DomainInterfaceDriver{
			Name:      hostInterfaceDriver,
			IOEventFD: "on",
		}
	}

	mtu := linkMtu(l)

	dom.Devices.Interfaces = append(dom.Devices.Interfaces,
		xlibvirt.DomainInterface{
			MAC: ifmac,
			Source: &xlibvirt.DomainInterfaceSource{
				Network: &xlibvirt.DomainInterfaceSourceNetwork{
					Network: net,
				},
			},
			Driver: driver,
			Model:  &xlibvirt.DomainInterfaceModel{Type: hostNicModel},
			Boot:   boot,
			Target: target,
			MTU:    &xlibvirt.DomainInterfaceMTU{Size: mtu},
		})
}

func resolveLinks(t *Topo) {

	// 'plug in' the link to each node
	for _, l := range t.Links {
		if l.IsTap() {
			continue
		}

		for _, e := range l.Endpoints {

			if e.Name == "" {
				if l.IsExternal() {
					continue
				}
			}

			h := t.GetHost(e.Name)
			if h == nil {
				log.Fatalf("could not find host '%s' - referenced by link", red(e.Name))
			}
			h.ports = append(h.ports, Port{l.Name, e.Port})
		}
	}

	// sort the links at each node by index
	for i := 0; i < len(t.Nodes); i++ {
		n := &t.Nodes[i]
		sort.Slice(n.ports, func(i, j int) bool {
			return n.ports[i].Index < n.ports[j].Index
		})
	}

}

func domainStatus(
	topo, name, qname string, conn *libvirt.Connect,
) DomStatus {

	var status DomStatus
	status.Name = name
	x, err := conn.LookupDomainByName(qname)
	if err != nil {
		status.State = "non-existant"
	} else {
		info, _ := x.GetInfo()
		switch info.State {
		case libvirt.DOMAIN_NOSTATE:
			status.State = "nostate"
		case libvirt.DOMAIN_RUNNING:
			status.State = "running"
			addrs, err := x.ListAllInterfaceAddresses(
				libvirt.DOMAIN_INTERFACE_ADDRESSES_SRC_LEASE)
			if err == nil {

				for _, a := range addrs {
					status.Macs = append(status.Macs, a.Hwaddr)
				}

				if len(addrs) > 0 {
					ifx := addrs[0]
					if len(ifx.Addrs) > 0 {
						status.IP = ifx.Addrs[0].Addr
					}
				}

			}
			status.ConfigState = configStatus(topo, name)
		case libvirt.DOMAIN_BLOCKED:
			status.State = "blocked"
		case libvirt.DOMAIN_PAUSED:
			status.State = "paused"
		case libvirt.DOMAIN_SHUTDOWN:
			status.State = "shutdown"
		case libvirt.DOMAIN_CRASHED:
			status.State = "crashed"
		case libvirt.DOMAIN_PMSUSPENDED:
			status.State = "suspended"
		case libvirt.DOMAIN_SHUTOFF:
			status.State = "off"
		}
		x.Free()
	}
	return status
}

func configStatus(topo, name string) string {
	dbCheckConnection()
	stateKey := fmt.Sprintf("config_state:%s:%s", topo, name)
	val, err := db.Get(stateKey).Result()
	if err == nil {
		return val
	}
	return ""
}

func networkStatus(name string, conn *libvirt.Connect, link Link) LinkStatus {

	result := LinkStatus{
		Link: link,
	}

	x, err := conn.LookupNetworkByName(name)
	if err != nil {
		result.State = "non-existant"
		return result
	}
	active, _ := x.IsActive()
	if active {
		result.State = "up"
		if link.IsExternal() {
			br, err := x.GetBridgeName()
			if err == nil {
				result.Bridge = br
			} else {
				log.WithError(err).Errorf("failed to get bridge name for external link")
			}
		} else if link.IsTap() { // TODO
			return result
		}
		return result
	}
	result.State = "down"
	return result
}

func destroyDomain(name string, conn *libvirt.Connect) {
	x, err := conn.LookupDomainByName(name)
	if err != nil {
		//ok nothing to destroy
	} else {
		x.Destroy()
		x.Undefine()
		x.Free()
	}
}

func shutdownDomain(name string, conn *libvirt.Connect) error {
	x, err := conn.LookupDomainByName(name)
	if err != nil {
		return fmt.Errorf("request to shutdown unknown vm %s", name)
	}
	x.ShutdownFlags(libvirt.DOMAIN_SHUTDOWN_ACPI_POWER_BTN)
	active, err := x.IsActive()
	if err == nil && active {
		log.Printf("shutting down %s", name)
		return x.Shutdown()
	}
	return err
}

func cleanupLinkNetwork(name string, conn *libvirt.Connect) {
	x, err := conn.LookupNetworkByName(name)
	if err != nil {
		//ok nothing to clean up
	} else {
		cleanupBOOTP(x)
	}
}

func cleanupTestNetwork(name string, conn *libvirt.Connect) {
	x, err := conn.LookupNetworkByName(name)
	if err != nil {
		//ok nothing to clean up
	} else {
		cleanupRPCBind(x)
	}
}

func destroyNetwork(name string, conn *libvirt.Connect) {
	x, err := conn.LookupNetworkByName(name)
	if err != nil {
		//ok nothing to destroy
	} else {
		x.Destroy()
		x.Undefine()
		x.Free()
	}
}

func setBridgeProperties(net *libvirt.Network, link Link) {

	if link.AllowLACP {
		/* WARNING: This requires a custom linux kernel as of 4.16
		 * See this commit:
		 * 		https://gitlab.com/rygoo/linux-lacp/commit/b3199fd1
		 */
		log.Warnf("Setting bridge up for LACP - this requires a custom kernel")
		log.Warnf("see: https://gitlab.com/rygoo/linux-lacp/commit/b3199fd1")
		setBridgeFwdMask(net, "16396")
	} else {
		setBridgeFwdMask(net, "16384")
	}

	allowBOOTP(net)
}

func setBridgeFwdMask(net *libvirt.Network, mask string) {
	name, _ := net.GetName()
	br, err := net.GetBridgeName()
	if err != nil {
		log.Printf("error getting bridge for %s - %v", name, err)
		return
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("/sys/class/net/%s/bridge/group_fwd_mask", br),
		[]byte(mask),
		0644,
	)

	if err != nil {
		log.Printf("unable to set group forwarding mask on bridge %s - %v",
			name,
			err,
		)
		return
	}
}

func allowBOOTP(net *libvirt.Network) {
	name, _ := net.GetName()
	br, err := net.GetBridgeName()
	if err != nil {
		log.Printf("error getting bridge for %s - %v", name, err)
		return
	}

	out, err := exec.Command("iptables", "-A", "FORWARD",
		"-i", br,
		"-d", "255.255.255.255",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error allowing bootp through iptables %s - %v", out, err)
		return
	}

}

func cleanupBOOTP(net *libvirt.Network) {
	name, _ := net.GetName()
	br, err := net.GetBridgeName()
	if err != nil {
		log.Printf("error getting bridge for %s - %v", name, err)
		return
	}

	out, err := exec.Command("iptables", "-D", "FORWARD",
		"-i", br,
		"-d", "255.255.255.255",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		// don't bother reporting bad rule, that just means the rule does not exist
		// and there is nothing to do. This can happen when a system is built but not
		// run for example because the iptables rules only get created on launch
		if strings.Contains(string(out), "Bad rule") {
			return
		}
		log.Printf("error cleaning bootp iptables rules %s - %v", out, err)
		return
	}

}

func allowRPCBind(net *libvirt.Network) {
	name, _ := net.GetName()
	br, err := net.GetBridgeName()
	if err != nil {
		log.Printf("error getting bridge for %s - %v", name, err)
		return
	}

	out, err := exec.Command("iptables", "-I", "INPUT",
		"-i", br,
		"-p", "tcp",
		"--dport", "111",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error allowing rpcbind tcp through iptables %s - %v", out, err)
		return
	}

	out, err = exec.Command("iptables", "-I", "INPUT",
		"-i", br,
		"-p", "udp",
		"--dport", "111",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error allowing rpcbind udp through iptables %s - %v", out, err)
		return
	}

	out, err = exec.Command("iptables", "-I", "INPUT",
		"-i", br,
		"-p", "tcp",
		"--dport", "2049",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error allowing nfs through iptables %s - %v", out, err)
		return
	}

}

func cleanupRPCBind(net *libvirt.Network) {
	name, _ := net.GetName()
	br, err := net.GetBridgeName()
	if err != nil {
		log.Printf("error getting bridge for %s - %v", name, err)
		return
	}

	out, err := exec.Command("iptables", "-D", "INPUT",
		"-i", br,
		"-p", "tcp",
		"--dport", "111",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		// don't bother reporting bad rule, that just means the rule does not exist
		// and there is nothing to do. This can happen when a system is built but not
		// run for example because the iptables rules only get created on launch
		if strings.Contains(string(out), "Bad rule") {
			return
		}
		log.Printf("error cleaning up iptables rpcbind tcp rule %s - %v", out, err)
		return
	}

	out, err = exec.Command("iptables", "-D", "INPUT",
		"-i", br,
		"-p", "udp",
		"--dport", "111",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error cleaning up iptables rpcbind udp rule %s - %v", out, err)
		return
	}

	out, err = exec.Command("iptables", "-D", "INPUT",
		"-i", br,
		"-p", "tcp",
		"--dport", "2049",
		"-j", "ACCEPT").CombinedOutput()

	if err != nil {
		log.Printf("error cleaning up nfs iptables tcp rule %s - %v", out, err)
		return
	}

}

func cleanupHostKey(ipaddr string) {
	out, err := exec.Command("ssh-keygen", "-R", ipaddr).CombinedOutput()
	if err != nil {
		log.Debugf("error cleaning up virtual machine hostkey: %s - %v", out, err)
	}
}

func linkMtu(link *Link) uint {
	// absent a link object, fall back to 1500
	var mtu uint = 1500

	if link != nil && !link.NoJumbo {
		mtu = 9216
	}

	return mtu
}

var red = color.New(color.FgRed).SprintFunc()
