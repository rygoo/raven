let name = "raven_"+Math.random().toString().substr(-6);

topo = {
  name: name,
  nodes: [deb("test-debian"), ubu("test-ubuntu")]
}

function deb(name) {
  return {
    name: name,
    image: "debian-bullseye",
    cpu: { cores: 4, passthru: true },
    mounts: [
	    {source: env.PWD+'/../../..', point: '/raven'},
    ],
  }
}

function ubu(name) {
  return {
    name: name,
    image: "ubuntu-2004",
    cpu: { cores: 4, passthru: true },
    mounts: [{source: env.PWD+'/../../..', point: '/raven'}]
  }
}
