#!/bin/bash

set -e

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

BLUE="\e[34m"
BLINK="\e[5m"
RESET="\e[0m"
stage() {
 echo -e "$BLUE$1$BLINK 🔨$RESET"
}

stage "destroying any exiting topologies"
rvn destroy

stage "building topology"
rvn build

stage "deploying topology"
rvn deploy

stage "waiting for topology to come up"
rvn pingwait test-ubuntu test-debian

stage "configuring topology"
rvn configure
rvn status

stage "test package install"
ansible-playbook -i .rvn/ansible-hosts configure.yml
