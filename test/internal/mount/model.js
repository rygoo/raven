topo = {
  name: "rvn-test",
  subnetoffset: 99,
  nodes: [deb("test")]
}

function deb(name) {
  return {
    name: name,
    image: "debian-buster",
    cpu: { cores: 2 },
    mounts: [{ source: env.PWD+'/data', point: '/tmp/data' }]
  }
}
